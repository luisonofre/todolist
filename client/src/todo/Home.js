import { Container, Box } from '@chakra-ui/react';
import { useEffect, useState } from "react";
import ToDoCreator from "./ToDoCreator";
import ToDoList from "./ToDoList";


function Home() {

    const [toDos, setToDos] = useState([]);

    const updateList = (list) => setToDos(list);

    useEffect(() => {
        const url = `/api/todo`;
        fetch(url)
            .then(response => {
                if (response.ok) return response.json();
                throw response;
            })
            .then(data => setToDos(data))
            .catch(err => console.log(err));
    }, []);

    function createToDo(name) {
        fetch(`/api/todo`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name: name })
        })
            .then(response => {
                if (response.ok) return response.json();
                throw response
            })
            .then(data => setToDos(old => [...old, data]))
            .catch(err => console.log(err));
    }


    return (
        <Container maxWidth='container.md' marginTop='20px'>
            <Box marginTop='20px'>
                <ToDoCreator createToDo={createToDo} />
            </Box>
            <Box marginTop='30px'>
                <ToDoList elements={toDos} updateList={updateList} />
            </Box>
        </ Container>
    )
}

export default Home;