import { Flex, Center, Text, Checkbox, IconButton, ButtonGroup } from '@chakra-ui/react';
import { EditIcon, DeleteIcon } from '@chakra-ui/icons';
import { useState } from 'react';
import ToDoEditor from './ToDoEditor';

/**
 * ToDo item that can be marked as done or edited
 */
function ToDo({ toDo, updateToDo, deleteToDo }) {
    const [name, setName] = useState(toDo.name);
    const [isChecked, setIsChecked] = useState(toDo.done);
    // EditMode helps to the change the UI
    const [editMode, setEditMode] = useState(false);

    const changeStatus = event => {
        const checked = event.target.checked;
        setIsChecked(checked);
        updateToDo({ ...toDo, done: checked })
    };


    function updateName(newName) {
        if (newName !== name) {
            setName(newName);
            updateToDo({ ...toDo, name: newName })
        }
        setEditMode(false);
    }

    if (editMode) {
        return (<ToDoEditor name={name} updateName={updateName} />)
    }

    return (
        <Flex height='50px' margin='10px' padding='10px'
            border="1px" borderColor="gray.200" _hover={{ backgroundColor: 'rgb(245,245,245)' }}>
            <Center width='10%'>
                <Checkbox isChecked={isChecked} onChange={changeStatus} />
            </Center>
            <Flex width='65%' alignItems='center' >
                <Text align='left' color={isChecked ? 'gray.400' : 'black'} decoration={isChecked ? 'line-through' : ''}>{name}</Text>
            </Flex>
            <Center width='25%'>
                <ButtonGroup>
                    <IconButton aria-label='' variant='ghost' icon={<EditIcon />} onClick={() => setEditMode(true)} />
                    <IconButton aria-label='' variant='ghost' colorScheme='red' icon={<DeleteIcon />} onClick={() => deleteToDo(toDo)} />
                </ButtonGroup>
            </Center>
        </Flex>
    );
}

export default ToDo;