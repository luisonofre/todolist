import { Flex, Box, Button } from '@chakra-ui/react';
import { useState } from 'react';
import ToDoInput from './ToDoInput';

function ToDoCreator({ createToDo }){

    const [name, setName] = useState('');
    const maxLength = 30;

    const getName = ( value ) => setName(value);

    const isDisabled = name.length > maxLength || name.length === 0;

    function onClick(){
        createToDo(name);
        setName('');
    }

    return(
        <Box>
            <Flex width='100%'>
                <ToDoInput getName={getName} maxLength={maxLength} defaultName={name} />
                <Button colorScheme='blue'  isDisabled={isDisabled} onClick={onClick} marginLeft='5px'>Create</Button>
            </Flex>
        </Box>
    )
}

export default ToDoCreator;