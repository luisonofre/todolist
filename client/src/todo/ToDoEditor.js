import { Box, Flex, Center, Button, ButtonGroup } from '@chakra-ui/react';
import { useState } from "react";
import ToDoInput from './ToDoInput';

function ToDoEditor({ name, updateName }) {
    const [newName, setNewName] = useState(name);
    const maxLength = 30;

    const getName = (value) => setNewName(value);
    const isDisabled = newName.length > maxLength || newName.length === 0;

    function cancel() {
        // Update the name with the current name
        // so there's no need to override
        updateName(name);
    }


    return (
        <Flex margin='10px' padding='10px'
            border="1px" borderColor="gray.200" _hover={{ backgroundColor: 'rgb(245,245,245)' }}>
            <Center width='65%'>
                <ToDoInput getName={getName} defaultName={name} maxLength={maxLength} />
            </Center>
            <Flex width='35%' marginLeft='10px' alignItems='center'>
                <ButtonGroup>
                    <Button colorScheme='blue' size='sm' variant='outline'
                        isDisabled={isDisabled} onClick={() => updateName(newName)} >Edit</Button>
                    <Button colorScheme='red' size='sm' onClick={cancel}>Cancel</Button>
                </ButtonGroup>
            </Flex>
        </Flex>
    );
}

export default ToDoEditor;