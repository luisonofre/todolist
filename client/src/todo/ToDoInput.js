import { Text, FormControl, Input, InputRightElement } from '@chakra-ui/react';
import { useEffect, useState } from 'react';

function ToDoInput({ getName, maxLength, defaultName }) {

    const [isInvalid, setIsInvalid] = useState(false);
    const [name, setName] = useState(defaultName ? defaultName : '');

    useEffect(() => {
        setIsInvalid(name.length > maxLength);
        getName(name);
        // eslint-disable-next-line
    }, [name]);

    useEffect(() => {
        // This is used to clear the input from 
        // the parent
        setName(defaultName)
    }, [defaultName]);

    const counter = (
        <Text color={name.length > maxLength ? 'red.500' : 'green.400'}
            margin='5px'>{name.length}/{maxLength}</Text>
    );

    return (
        <FormControl isInvalid={isInvalid} >
            <Input variant='filled' placeholder='Write something to do'
                type='text' value={name} onChange={(e) => setName(e.target.value)} />
            <InputRightElement children={counter} />
        </FormControl>
    )
}

export default ToDoInput;