import { VStack, StackDivider, Box, Text, Center } from '@chakra-ui/react';
import ToDo from "./ToDo";
import { useEffect, useState } from "react";

function ToDoList({ elements, updateList }) {

    const [toDos, setToDos] = useState(elements);

    useEffect(() => {
        // Used to render again when a To Do is inserted 
        // from the parent
        setToDos(elements)
    }, [elements]);

    useEffect(() => {
        // To register changes in the parent
        updateList(toDos);
    }, [toDos]);

    function updateToDo(toDo) {
        fetch(`/api/todo/${toDo._id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(toDo)
        })
            .then(response => {
                if (response.ok) return response.json();
                throw response
            })
            .then(data => {
                setToDos(oldToDos =>
                    oldToDos.map(item => {
                        if (item._id !== data._id) return item;
                        return { ...item, ...data };
                    })
                );
            })
            .catch(err => console.log(err));

    }

    function deleteToDo(toDo) {
        fetch(`/api/todo/${toDo._id}`, { method: 'DELETE' })
            .then(response => {
                if (response.ok) {
                    setToDos(oldToDos =>
                        oldToDos.filter(item => item._id !== toDo._id)
                    );
                }
                throw response
            })
            .catch(err => console.log(err));
    }


    const createToDo = (toDo) =>
        <ToDo toDo={toDo} key={toDo._id} updateToDo={updateToDo} deleteToDo={deleteToDo} />;

    const complete = toDos.filter(toDo => toDo.done).map(createToDo);
    const incomplete = toDos.filter(toDo => !toDo.done).map(createToDo);

    const centeredMessage = (message) => (
        <Center>
            <Text color='gray.500'>{message}</Text>
        </Center>
    );

    return (
        <Box>
            <VStack divider={<StackDivider borderColor='gray.300' />}>
                <Box width='100%'>
                    <Text fontSize='2xl'>To Do</Text>
                    {incomplete.length > 0 ? incomplete : centeredMessage("You've done all your tasks 🔥 ")}
                </Box>
                <Box width='100%'>
                    <Text fontSize='2xl'>Complete tasks</Text>
                    {complete.length > 0 ? complete : centeredMessage('Nothing to show here')}
                </Box>
            </VStack>
        </Box>
    )
}

export default ToDoList;