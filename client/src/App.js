import './App.css';
import Home from './todo/Home';

function App() {

    return (
        <div className="App">
            <Home />
        </div>
    );
}

export default App;
