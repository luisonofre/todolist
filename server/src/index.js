const express =  require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');
const router = require('./routes');

if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}

const app = express();
const port = process.env.PORT || 3000;

const password =  process.env.MONGO_PASS;
const user = process.env.MONGO_USER;
const mongoUri = `mongodb+srv://${user}:${password}@cluster0.kaygt.mongodb.net/raccoon?retryWrites=true&w=majority`;

mongoose.connect(mongoUri)
    .catch(err => console.log(err.reason));

app.use(cors());    
app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use(express.static(path.resolve(__dirname, '../../client/build')));

app.use('/api', router);

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../client/build', 'index.html'));
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port} 🚀🚀`);
});