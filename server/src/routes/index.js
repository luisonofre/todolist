const express =  require('express');
const toDoController =  require('../toDo/ToDoController');

const router = express.Router();

router.get('/todo', toDoController.all);
router.get('/todo/:id', toDoController.byId);
router.post('/todo', toDoController.create);
router.put('/todo/:id', toDoController.update);
router.delete('/todo/:id', toDoController.deleteOne);

module.exports = router;