const ToDo = require('./ToDo');


function all(req, res){
    ToDo.find({})
        .exec((err, toDos) => res.json(toDos));
}

function byId(req, res){
    const id = req.params.id;
    ToDo.findById(id, (err, toDo) => {
        if(!toDo) return res.status(404).json({});
        res.json(toDo);
    })
}

function create(req, res){
    const { name }  = req.body;
    const newToDo = new ToDo({name, done: false});
    newToDo.save((err, saved) => {
        res.json(saved);
    });
}

function update(req, res){
    const id = req.params.id;
    const { name, done} = req.body;
    ToDo.findById(id, (err, toDo) => {
        if(!toDo) return res.status(404).json({});

        toDo.name = name;
        toDo.done = done;
        toDo.save((err, updated) => {
            res.json(updated);
        });
    });
}

function deleteOne(req, res){
    const id = req.params.id;
    ToDo.deleteOne({_id: id}, (err, deletedCount) => {
        res.status(204).send();
    });
}

const toDoController = {
    all, 
    byId,
    create,
    update,
    deleteOne
};

module.exports = toDoController;