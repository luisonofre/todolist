const { Schema, model } = require("mongoose");

const schema = new Schema({
    name: 'string',
    done: 'boolean'
});

const ToDo = model('ToDo', schema);
module.exports = ToDo;
