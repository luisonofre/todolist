# 🦝  To Do List
## Tecnologías ocupadas
- React
- Chackra UI
- Express
- Mongoose
- MongoDB

## ¿Cómo correr la aplicación?

Primero hay que declarar las siguientes variables de entorno:

```ba
export MONGO_USER=<atlas-db-user>
export MONGO_PASS=<password>
```

con esto nos podemos conectar a la Base de Datos externa.



Después en este directorio, correr:

```ba
npm run start
```

el cual va a instalar las dependencias y correr el programa.
